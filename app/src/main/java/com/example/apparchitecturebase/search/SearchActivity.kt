package com.example.apparchitecturebase.search

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apparchitecturebase.R
import com.example.apparchitecturebase.base.BaseActivity
import com.example.apparchitecturebase.databinding.ActivitySearchBinding

class SearchActivity : BaseActivity<ActivitySearchBinding, SearchViewModel>(R.layout.activity_search) {

    private val adapter = SearchResultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, SearchViewModel.Factory(binding.searchResultList.layoutManager as? LinearLayoutManager)).get(SearchViewModel::class.java)
        viewModel.searchResults.observe(this, Observer { resource ->
            if (handleResource(resource)) {
                if (viewModel.page == SearchViewModel.FIRST_PAGE) {
                    //Scroll on top of the list
                    (binding.searchResultList.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(0, 0)
                    //Clear the previous search results
                    adapter.clearDataSet()
                    //Reset the scroll listener state
                    viewModel.infiniteScrollListener.resetState()
                }
                if (resource?.data?.searchResults != null)
                    adapter.addToDataSet(resource.data.searchResults!!)
            }
        })

        binding.viewModel = viewModel
        binding.searchResultList.adapter = adapter

    }

    override fun getTitleString(): String? {
        return "Search for movies"
    }

    override fun showHomeButton(): Boolean {
        return false
    }
}