package com.example.apparchitecturebase.search

import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apparchitecturebase.base.BaseViewModel
import com.example.apparchitecturebase.commons.InfiniteScrollListener
import com.example.apparchitecturebase.datakit.model.Resource
import com.example.apparchitecturebase.datakit.model.SearchResultResponse
import com.example.apparchitecturebase.datakit.repository.MovieRepository

class SearchViewModel(layoutManager: LinearLayoutManager?) : BaseViewModel() {
    companion object {
        const val FIRST_PAGE = 1
    }

    private val movieRepository = MovieRepository()
    private val pageTrigger: MutableLiveData<Int> = MutableLiveData()

    var page = FIRST_PAGE
    var query: String? = null

    val searchResults: LiveData<Resource<SearchResultResponse>> = Transformations
            .switchMap(pageTrigger) {
                movieRepository.searchForMovies(query, it)
            }

    val onEditorActionListener = TextView.OnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            page = FIRST_PAGE
            pageTrigger.value = page
            true
        } else false
    }

    val infiniteScrollListener = InfiniteScrollListener({
        page++
        pageTrigger.value = page
    }, layoutManager)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val layoutManager: LinearLayoutManager?) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SearchViewModel(layoutManager) as T
        }
    }
}