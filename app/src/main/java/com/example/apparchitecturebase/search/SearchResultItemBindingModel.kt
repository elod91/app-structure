package com.example.apparchitecturebase.search

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.example.apparchitecturebase.databinding.ItemBindingModel
import com.example.apparchitecturebase.datakit.model.SearchResult
import com.example.apparchitecturebase.moviedetails.MovieDetailsActivity

class SearchResultItemBindingModel(item: SearchResult) : ItemBindingModel<SearchResult>(item) {

    fun openMovieDetails(context: Context) {
        if (context is Activity) {
            val openMovieDetails = Intent(context, MovieDetailsActivity::class.java)
            openMovieDetails.putExtra(MovieDetailsActivity.FLAG_MOVIE_TITLE, item.title)
            openMovieDetails.putExtra(MovieDetailsActivity.FLAG_IMDB_ID, item.imdbID)

            context.startActivity(openMovieDetails)
        }
    }
}