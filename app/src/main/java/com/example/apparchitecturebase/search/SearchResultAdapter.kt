package com.example.apparchitecturebase.search

import com.example.apparchitecturebase.R
import com.example.apparchitecturebase.base.BaseAdapter
import com.example.apparchitecturebase.databinding.ItemSearchResultBinding
import com.example.apparchitecturebase.datakit.model.SearchResult

class SearchResultAdapter : BaseAdapter<ItemSearchResultBinding, SearchResult>(R.layout.item_search_result) {

    fun addToDataSet(searchResult: List<SearchResult>) {
        (data as? ArrayList)?.addAll(searchResult)
        notifyItemRangeInserted(data.size - searchResult.size, searchResult.size)
    }

    fun clearDataSet() {
        data = ArrayList()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder<ItemSearchResultBinding>, position: Int) {
        holder.binding.bindingModel = SearchResultItemBindingModel(data[position])
    }
}