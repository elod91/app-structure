package com.example.apparchitecturebase.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.example.apparchitecturebase.R

class ProgressLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    private var progressBar: ProgressBar

    init {
        val progressBarLayoutParams = FrameLayout.LayoutParams(resources.getDimensionPixelSize(R.dimen.grid_6), resources.getDimensionPixelSize(R.dimen.grid_6))
        progressBarLayoutParams.gravity = Gravity.CENTER_HORIZONTAL
        progressBarLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.grid_2)

        progressBar = ProgressBar(context)
        progressBar.setBackgroundResource(R.drawable.bg_circle_shadow)
        progressBar.setPadding(resources.getDimensionPixelSize(R.dimen.grid_1), resources.getDimensionPixelSize(R.dimen.grid_1), resources.getDimensionPixelSize(R.dimen.grid_1), resources.getDimensionPixelSize(R.dimen.grid_1))
        progressBar.visibility = View.GONE

        addView(progressBar, progressBarLayoutParams)
    }

    fun setShowProgress(showProgress: Boolean) {
        progressBar.bringToFront()
        progressBar.visibility = if (showProgress) View.VISIBLE else View.GONE
    }
}