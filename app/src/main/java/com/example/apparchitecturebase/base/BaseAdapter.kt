package com.example.apparchitecturebase.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<B : ViewDataBinding, I>(private var layoutRes: Int) : RecyclerView.Adapter<BaseAdapter.ViewHolder<B>>() {
    protected var data: List<I> = ArrayList()

    fun setDataSet(data: List<I>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<B> {
        val binding = DataBindingUtil.inflate<B>(
                LayoutInflater.from(parent.context),
                layoutRes,
                parent,
                false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder<B : ViewDataBinding>(var binding: B) : RecyclerView.ViewHolder(binding.root)

}