package com.example.apparchitecturebase.base

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.apparchitecturebase.datakit.model.Resource
import com.example.apparchitecturebase.datakit.model.Status


abstract class BaseActivity<B : ViewDataBinding, VM : BaseViewModel>(private val layoutRes: Int) : AppCompatActivity() {
    protected lateinit var binding: B
    protected lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutRes)
        binding.setLifecycleOwner(this)

        supportActionBar?.let {
            it.title = getTitleString()
            it.setDisplayHomeAsUpEnabled(showHomeButton())
            it.setDisplayShowHomeEnabled(showHomeButton())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()

        return true
    }

    protected fun handleResource(resource: Resource<*>?): Boolean =
            when (resource?.status) {
                Status.LOADING -> {
                    currentFocus?.let {
                        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
                    }

                    viewModel.showProgress.value = true
                    false
                }
                Status.ERROR -> {
                    viewModel.showProgress.value = false
                    showErrorDialog(resource.message)
                    false
                }
                Status.SUCCESS -> {
                    viewModel.showProgress.value = false
                    true
                }
                else -> {
                    false
                }
            }

    private fun showErrorDialog(message: String?) {
        AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                .show()
    }

    open fun showHomeButton(): Boolean = true

    abstract fun getTitleString(): String?
}