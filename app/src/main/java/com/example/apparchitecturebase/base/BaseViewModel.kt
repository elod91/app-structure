package com.example.apparchitecturebase.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    val showProgress: MutableLiveData<Boolean> = MutableLiveData()
}