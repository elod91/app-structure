package com.example.apparchitecturebase.datakit.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.apparchitecturebase.AppExecutors
import com.example.apparchitecturebase.datakit.model.Resource
import com.example.apparchitecturebase.datakit.util.ApiEmptyResponse
import com.example.apparchitecturebase.datakit.util.ApiErrorResponse
import com.example.apparchitecturebase.datakit.util.ApiResponse
import com.example.apparchitecturebase.datakit.util.ApiSuccessResponse

abstract class NetworkBoundResource<RequestType>
@MainThread constructor() {
    private val result = MediatorLiveData<Resource<RequestType>>()

    init {
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    AppExecutors.networkIO.execute {
                        AppExecutors.mainThread.execute {
                            setValue(Resource.success(processResponse(response)))
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    AppExecutors.mainThread.execute {
                        // reload from disk whatever we had
                        setValue(Resource.error("Empty response"))
                    }
                }
                is ApiErrorResponse -> {
                    onFetchFailed()
                    AppExecutors.mainThread.execute {
                        // reload from disk whatever we had
                        setValue(Resource.error(response.errorMessage))
                    }
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestType>) = response.body

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}
