package com.example.apparchitecturebase.datakit.api

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateAdapter : JsonAdapter<Date>() {
    private val dateFormat1: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    private val dateFormat2: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    init {
        dateFormat1.timeZone = TimeZone.getTimeZone("GMT")
        dateFormat2.timeZone = TimeZone.getTimeZone("GMT")
    }

    @Synchronized
    @Throws(IOException::class)
    override fun fromJson(reader: JsonReader): Date? {
        val string = reader.nextString()
        return try {
            dateFormat1.parse(string)
        } catch (e: Exception) {
            dateFormat2.parse(string)
        }
    }

    @Synchronized
    @Throws(IOException::class)
    override fun toJson(writer: JsonWriter, value: Date?) {
        val string = dateFormat1.format(value)
        writer.value(string)
    }
}