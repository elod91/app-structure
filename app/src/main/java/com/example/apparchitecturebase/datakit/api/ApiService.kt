package com.example.apparchitecturebase.datakit.api

import androidx.lifecycle.LiveData
import com.example.apparchitecturebase.datakit.model.Movie
import com.example.apparchitecturebase.datakit.model.SearchResultResponse
import com.example.apparchitecturebase.datakit.util.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

//These have hardcoded query params added for simplicity
interface ApiService {
    companion object {
        const val API_KEY = "411f9106"
    }

    @GET(".")
    fun searchForMovies(@Query("apikey") apiKey: String = API_KEY, @Query("s") query: String?, @Query("page") page: Int?): LiveData<ApiResponse<SearchResultResponse>>

    @GET(".")
    fun getMovieDetails(@Query("apikey") apiKey: String = API_KEY, @Query("i") imdbID: String?): LiveData<ApiResponse<Movie>>
}