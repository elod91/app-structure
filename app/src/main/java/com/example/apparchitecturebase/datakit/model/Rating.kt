package com.example.apparchitecturebase.datakit.model

import com.squareup.moshi.Json

data class Rating(
        @Json(name = "Source")
        var source: String? = null,
        @Json(name = "Value")
        var value: String? = null
)