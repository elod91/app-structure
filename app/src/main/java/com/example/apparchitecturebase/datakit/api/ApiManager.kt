package com.example.apparchitecturebase.datakit.api

import com.example.apparchitecturebase.Application
import com.example.apparchitecturebase.datakit.util.LiveDataCallAdapterFactory
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


object ApiManager {
    private const val BASE_URL: String = "https://www.omdbapi.com/"
    private const val TIMEOUT = 60L

    private val client: OkHttpClient.Builder

    lateinit var apiService: ApiService

    init {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        client = OkHttpClient.Builder().apply {
            connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            readTimeout(TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            networkInterceptors().add(Interceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder().header("Accept", "application/json").method(original.method(), original.body())
                chain.proceed(request.build())
            })
            addInterceptor(interceptor)
            addInterceptor(provideOfflineCacheInterceptor())
            Application.Cache.DIR?.let {
                cache(provideCache())
            }

        }
        initServices(initRetrofit())
    }

    private fun initRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(createGsonConverter())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(client.build())
                .build()
    }

    private fun createGsonConverter(): MoshiConverterFactory {
        return MoshiConverterFactory.create(Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .add(Date::class.java, DateAdapter())
                .build()).asLenient()
    }

    private fun initServices(retrofit: Retrofit) {
        apiService = retrofit.create(ApiService::class.java)
    }

    private fun provideCache(): Cache? {
        var cache: Cache? = null
        try {
            cache = Cache(File(Application.Cache.DIR, "http-cache"), 40 * 1024 * 1024) // 40 MB
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return cache
    }

    private fun provideOfflineCacheInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            val cacheControl = CacheControl.Builder().maxStale(10, TimeUnit.MINUTES).build()
            try {
                request = request.newBuilder().cacheControl(cacheControl).build()
            } catch (e: Exception) {
            }
            chain.proceed(request)
        }
    }
}