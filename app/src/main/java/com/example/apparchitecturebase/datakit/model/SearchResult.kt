package com.example.apparchitecturebase.datakit.model

import com.squareup.moshi.Json

data class SearchResult(
        @Json(name = "Title")
        var title: String? = null,
        @Json(name = "Year")
        var year: String? = null,
        @Json(name = "imdbID")
        var imdbID: String? = null,
        @Json(name = "Type")
        var type: String? = null,
        @Json(name = "Poster")
        var poster: String? = null
)