package com.example.apparchitecturebase.datakit.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
