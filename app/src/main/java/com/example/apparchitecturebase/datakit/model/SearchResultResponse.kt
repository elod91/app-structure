package com.example.apparchitecturebase.datakit.model

import com.squareup.moshi.Json

class SearchResultResponse(
        @Json(name = "Search")
        var searchResults: List<SearchResult>? = null,
        @Json(name = "totalResults")
        var totalResult: String? = null,
        @Json(name = "Response")
        var response: String? = null
)