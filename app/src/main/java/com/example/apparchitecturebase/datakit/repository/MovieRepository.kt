package com.example.apparchitecturebase.datakit.repository

import androidx.lifecycle.LiveData
import com.example.apparchitecturebase.datakit.api.ApiManager
import com.example.apparchitecturebase.datakit.model.Movie
import com.example.apparchitecturebase.datakit.model.Resource
import com.example.apparchitecturebase.datakit.model.SearchResultResponse
import com.example.apparchitecturebase.datakit.util.ApiResponse

class MovieRepository {
    fun searchForMovies(query: String?, page: Int?): LiveData<Resource<SearchResultResponse>> {
        return object : NetworkBoundResource<SearchResultResponse>() {
            override fun createCall(): LiveData<ApiResponse<SearchResultResponse>> {
                return ApiManager.apiService.searchForMovies(query = query, page = page)
            }
        }.asLiveData()
    }

    fun getMovieDetails(imdbID: String?): LiveData<Resource<Movie>> {
        return object : NetworkBoundResource<Movie>() {
            override fun createCall(): LiveData<ApiResponse<Movie>> {
                return ApiManager.apiService.getMovieDetails(imdbID = imdbID)
            }
        }.asLiveData()
    }
}