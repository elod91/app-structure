package com.example.apparchitecturebase.databinding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.apparchitecturebase.GlideApp

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("bind:imageUrl")
    fun ImageView.setImage(imageUrl: String?) {
        GlideApp.with(this)
                .load(imageUrl)
                .into(this)
    }

    @JvmStatic
    @BindingAdapter("bind:viewVisible")
    fun View.setViewVisible(visible: Boolean) {
        visibility = if (visible) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("bind:singleOnScrollListener")
    fun RecyclerView.setSingleOnScrollListener(scrollListener: RecyclerView.OnScrollListener) {
        clearOnScrollListeners()
        addOnScrollListener(scrollListener)
    }
}