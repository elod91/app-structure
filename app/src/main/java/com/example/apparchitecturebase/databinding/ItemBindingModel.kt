package com.example.apparchitecturebase.databinding

import androidx.databinding.BaseObservable

open class ItemBindingModel<I>(val item: I) : BaseObservable()