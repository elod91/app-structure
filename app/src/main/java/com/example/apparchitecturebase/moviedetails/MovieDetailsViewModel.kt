package com.example.apparchitecturebase.moviedetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.apparchitecturebase.base.BaseViewModel
import com.example.apparchitecturebase.datakit.model.Movie
import com.example.apparchitecturebase.datakit.model.Resource
import com.example.apparchitecturebase.datakit.repository.MovieRepository

class MovieDetailsViewModel : BaseViewModel() {
    private val movieRepository = MovieRepository()
    private val refreshTrigger: MutableLiveData<Boolean> = MutableLiveData()

    var imdbID: String? = null
        set(value) {
            field = value
            refreshData()
        }

    val movieDetails: LiveData<Resource<Movie>> = Transformations
            .switchMap(refreshTrigger) {
                movieRepository.getMovieDetails(imdbID)
            }

    fun refreshData() {
        if (imdbID != null)
            refreshTrigger.value = true
    }
}