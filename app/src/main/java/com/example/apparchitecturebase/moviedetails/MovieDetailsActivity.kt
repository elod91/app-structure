package com.example.apparchitecturebase.moviedetails

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.apparchitecturebase.R
import com.example.apparchitecturebase.base.BaseActivity
import com.example.apparchitecturebase.databinding.ActivityMovieDetailsBinding

class MovieDetailsActivity : BaseActivity<ActivityMovieDetailsBinding, MovieDetailsViewModel>(R.layout.activity_movie_details) {
    companion object {
        const val FLAG_MOVIE_TITLE = "movie-title"
        const val FLAG_IMDB_ID = "imdb-id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel::class.java)
        viewModel.movieDetails.observe(this, Observer { resource ->
            handleResource(resource)
        })
        viewModel.imdbID = intent.getStringExtra(FLAG_IMDB_ID)

        binding.viewModel = viewModel
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_refresh, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh_data) {
            viewModel.refreshData()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getTitleString(): String? {
        return intent.getStringExtra(FLAG_MOVIE_TITLE)
    }
}